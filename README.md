Telegram && Vkontakte (soon...) Bots For Schedule

## Getting started

### Configuration
Customize the configuration by editing the `appsettings.json` (in the [TVBFS.Host](src/TVBFS.Host)) file parameters as desired:

| Parameter | Description |
|-|-|
| `BOT_TOKEN` | Your Telegram bot's token, obtained using [BotFather](http://t.me/botfather) (check [tutorial](https://core.telegram.org/bots/tutorial#obtain-your-bot-token)) |
| `BOT_HOST_ADDRESS` | Url that will receive updates from Telegram server (see https://core.telegram.org/bots/webhooks or you can use something like [ngrok](https://ngrok.com/docs/getting-started) (or [lt](https://theboroer.github.io/localtunnel-www)))
| `SCHEDULE_LOGIN` | Login credentials for signing into schedule site (note that this site should be from [IRTech](https://www.ir-tech.ru)). Account needs to be with admin rights.
| `SCHEDULE_PASSWORD` | Password from the schedule site account. As i say, account needs to be with admin rights.
| `PLAIN_DOMAIN` | Schedule site domain (e.g. `www.domain.com`)
| `LOGIN_URL` | Schedule site login page (or API) (e.g. `www.domain.com/api/login`)
| `ALL_GROUPS_URL` | Url that contains info about all groups (e.g. `www.domain.com/groups/all`)
| `OWNER_TELEGRAM_USERNAME` | Username of the owner (should be without `@` sign)
| `OWNER_TELEGRAM_ID` | Id of the owner (can get from the [Json Dump Bot](https://t.me/JsonDumpBot) in `message:from:id`)
| `ConnectionStrings`| Configuration of both of them can be found in [MSDN](https://learn.microsoft.com/en-us/ef/core/miscellaneous/connection-strings)

### Optional configuration
| Parameter | Description | Default value |
|-|-|-|
| `CallbackHandlerKeyLength` | Length of the generated hash | 10

### Solution structure
- [TVBFS.Host](src/TVBFS.Host) — web app to host Telegram webhooks and run the application
- [TVBFS.Application](src/TVBFS.Application) — application layer with manipulations with schedule and everything which needed to handle a Telegram update
- [TVBFS.Domain](src/TVBFS.Domain) — domain models and services that do not depent on anything including Telegram APIs
- [TVBFS.Persistence](src/TVBFS.Persistence) — persistence layer containing database contexts

### Technologies
* [ASP.NET Core 7](https://docs.microsoft.com/en-us/aspnet/core/introduction-to-aspnet-core)
* [Entity Framework Core 7](https://docs.microsoft.com/en-us/ef/core/)
* [MediatR](https://github.com/jbogard/MediatR)
* [Hangfire](https://github.com/HangfireIO/Hangfire)
* [Mapster](https://github.com/MapsterMapper/Mapster)
* [Serilog](https://github.com/serilog/serilog)
* [Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json)

### Useful links
- [ZdSchedule](https://t.me/zd_schbot) — the bot
- [Leet](https://t.me/be_funge) — developer (me)
