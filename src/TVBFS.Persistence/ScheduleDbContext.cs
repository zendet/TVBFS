using Microsoft.EntityFrameworkCore;
using TVBFS.Domain.Models;
using TVBFS.Domain.Models.Entities;

namespace TVBFS.Persistence;

public sealed class ScheduleDbContext : DbContext
{
    public DbSet<User> Users { get; set; } = default!;
    public DbSet<TelegramUser> TelegramUsers { get; set; } = default!;
    public DbSet<VkontakteUser> VkontakteUsers { get; set; } = default!;
    public DbSet<State> States { get; set; } = null!;
    public DbSet<Lesson> Lessons { get; set; } = default!;
    public DbSet<Group> Groups { get; set; } = default!;
    public DbSet<CachedCookie> CachedCookies { get; set; } = default!;
        
    public ScheduleDbContext(DbContextOptions<ScheduleDbContext> options) : base(options)
    {
        Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.EnableSensitiveDataLogging(); // will disable it later
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
            .HasOne(t => t.TelegramUser)
            .WithOne(u => u.User)
            .HasForeignKey<TelegramUser>(t => t.Id);

        modelBuilder.Entity<User>()
            .HasOne(v => v.VkontakteUser)
            .WithOne(u => u.User)
            .HasForeignKey<VkontakteUser>(v => v.Id);
    }
}