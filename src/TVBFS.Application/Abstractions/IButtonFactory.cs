using Telegram.Bot.Types.ReplyMarkups;

namespace TVBFS.Application.Abstractions;

public interface ICallbackButtonFactory
{
    public InlineKeyboardButton CreateCallbackDataButton(string text, string data);
}