namespace TVBFS.Application.Abstractions;

public interface IReplyMemoryService
{
    public void SetPreviousReplyMessageId(string handlerName, long chatId, int messageId);
    public int? TryGetPreviousReplyMessageId(string handlerName, long chatId);
}