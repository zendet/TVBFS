namespace TVBFS.Application.Abstractions;

public interface IHashingService
{
    public string Convert(string key);
}