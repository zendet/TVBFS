namespace TVBFS.Application.Abstractions;

public interface IBase64ConverterService
{
    public string Convert(string text);
}