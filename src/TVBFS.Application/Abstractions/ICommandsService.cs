using Microsoft.Extensions.DependencyInjection;

namespace TVBFS.Application.Abstractions;

public interface ICommandsService
{
    public IBotCommand GetCommandInstance(IServiceScope scope, string commandName);
    public string? GetCommandByName(string name);
    public string? GetCommandNameBySlash(string slash);
}