using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Abstractions;

public interface ICallbackQueryHandler
{
    public Task HandleCallbackQueryAsync(CallbackQueryContext context, CancellationToken token);
}