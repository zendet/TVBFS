using TVBFS.Application.Models;
using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Abstractions;

public interface IBotCommand
{
    public Task ExecuteAsync(CommandContext context, CancellationToken cancellationToken);
}