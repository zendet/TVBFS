using TVBFS.Application.Models;

namespace TVBFS.Application.Abstractions;

public interface ILinguisticParser
{
    public CaseParsingResult? TryParseName(string text);
}