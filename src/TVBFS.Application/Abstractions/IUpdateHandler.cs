using Telegram.Bot.Types;

namespace TVBFS.Application.Abstractions;

public interface IUpdateHandler
{
    public Task HandleAsync(Update update);
}