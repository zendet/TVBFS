using TVBFS.Application.Models;
using TVBFS.Application.Models.Resources;

namespace TVBFS.Application.Abstractions;

public interface IResourcesService
{
    public CommandResources? GetCommandResources<TCommand>() where TCommand : IBotCommand;
    public CommandResources? GetCommandResources(string commandName);
    public BehaviorResources? GetBehaviorResources(string behaviorName);
    // public CallbackResources? GetCallbackResources(string callbackName);
}
