using TVBFS.Application.Delegates;
using TVBFS.Application.Models;
using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Abstractions;

public interface IBehavior
{
    public Task HandleAsync(BehaviorContext context, BehaviorContextHandler next);
} 