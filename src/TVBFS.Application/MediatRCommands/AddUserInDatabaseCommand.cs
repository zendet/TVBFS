using MediatR;
using TVBFS.Application.Models.Contexts;
using TVBFS.Domain.Models;
using TVBFS.Domain.Models.Entities;

namespace TVBFS.Application.MediatRCommands;

public record AddOrUpdateUserInDatabaseCommand(BehaviorContext BehaviorContext) : IRequest<User>;