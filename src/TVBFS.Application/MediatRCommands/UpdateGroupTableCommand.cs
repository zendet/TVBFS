using MediatR;
using TVBFS.Application.Models;

namespace TVBFS.Application.MediatRCommands;

public record UpdateGroupTableCommand(List<UserGroup> UserGroups) : IRequest<List<UserGroup>>;