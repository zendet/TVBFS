using MediatR;
using TVBFS.Domain.Models.Entities;

namespace TVBFS.Application.MediatRCommands;

public record UpdateUserSubscriptionCommand(long UserId, bool Subscription) : IRequest<User>;
