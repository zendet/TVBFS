using MediatR;
using TVBFS.Domain.Models.Entities;

namespace TVBFS.Application.MediatRCommands;

public record AddOrUpdateScheduleCommand(List<string> DbGroupNames, bool UpdateSchedule) : IRequest<List<User>>;