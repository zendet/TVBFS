using MediatR;
using TVBFS.Domain.Models.Entities;

namespace TVBFS.Application.MediatRCommands;

public record AddUserInGroupCommand(long UserId, string GroupName) : IRequest<User>;