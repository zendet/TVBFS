using MediatR;
using TVBFS.Application.Models.Contexts;
using TVBFS.Domain.Models;

namespace TVBFS.Application.MediatRCommands;

// public record UpdateLastSeenStatusCommand(BehaviorContext ChatId) : IRequest<User>;