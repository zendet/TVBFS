using Newtonsoft.Json;

namespace TVBFS.Application.Models;

public class UserGroup
{
    [JsonProperty("name")]
    public string Title { get; set; }
    [JsonProperty("id")]
    public int Id { get; set; }

}