namespace TVBFS.Application.Models.Checkpoints;

public record Checkpoint(string Name, string HandlerName, long? ChatId);