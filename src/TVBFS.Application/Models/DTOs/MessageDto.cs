namespace TVBFS.Application.Models.DTOs;

public class MessageDto
{
    public int Id { get; set; }
    public string Text { get; set; } = null!;
    public UserDto Sender { get; set; } = null!;
    public ChatDto Chat { get; set; } = null!;
}