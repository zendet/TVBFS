namespace TVBFS.Application.Models;

public record CaseParsingResult(string Case);
