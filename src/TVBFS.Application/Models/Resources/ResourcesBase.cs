using System.Globalization;
using Newtonsoft.Json.Linq;

namespace TVBFS.Application.Models.Resources;

public abstract class ResourcesBase
{
    private readonly JObject _data;

    protected ResourcesBase(JObject data) =>
        _data = data;

    public T Get<T>(string key) =>
        _data.GetValue(key)!.Value<T>()!;
    
    public string Get(string key) =>
        string.Format(CultureInfo.InvariantCulture, Get<string>(key));
    
    public IEnumerable<T?> GetManyOrEmpty<T>(string key) =>
        _data.GetValue(key)?.Values<T>() ?? Enumerable.Empty<T>();
}