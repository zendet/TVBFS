using Newtonsoft.Json.Linq;

namespace TVBFS.Application.Models.Resources;

public class CallbackResources : ResourcesBase
{
    public CallbackResources(JObject data)
        : base(data) { }
}