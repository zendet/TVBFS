using Newtonsoft.Json.Linq;

namespace TVBFS.Application.Models.Resources;

public class CommandResources : ResourcesBase
{
    public CommandResources(JObject data) 
        : base(data) { }

    public IEnumerable<string> Names =>
        GetManyOrEmpty<string>("Names")!;

    public IEnumerable<string> Slashes =>
        GetManyOrEmpty<string>("Slashes")!;
}