using Newtonsoft.Json.Linq;

namespace TVBFS.Application.Models.Resources;

public class BehaviorResources : ResourcesBase
{
    public BehaviorResources(JObject data) 
        : base(data) { }
}