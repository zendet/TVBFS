using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Models.Resources;

namespace TVBFS.Application.Models.Contexts;

public class BehaviorContext : ContextBase<BehaviorResources>
{
    public BehaviorContext(
        ITelegramBotClient bot,
        ICheckpointMemoryService checkpoint,
        IReplyMemoryService replies)
        : base(bot, checkpoint, replies) { }
}
