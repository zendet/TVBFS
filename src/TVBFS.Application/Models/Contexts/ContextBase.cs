using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Models.Checkpoints;
using TVBFS.Application.Models.DTOs;
using TVBFS.Application.Models.Resources;

namespace TVBFS.Application.Models.Contexts;

public abstract class ContextBase<TResources> where TResources : ResourcesBase
{
    public string HandlerName = null!;
    public UpdateDto Update = null!;
    public TResources? Resources;
    
    private readonly ITelegramBotClient _bot;
    private readonly ICheckpointMemoryService _checkpoints;
    private readonly IReplyMemoryService _replies;

    public ContextBase(
        ITelegramBotClient bot,
        ICheckpointMemoryService checkpoints, 
        IReplyMemoryService replies)
    {
        _bot = bot;
        _checkpoints = checkpoints;
        _replies = replies;
    }
    
    public void SetCheckpoint(string name, TimeSpan? duration = null)
    {
        var checkpoint = new Checkpoint(name, HandlerName, Update.ChatId);
        SetCheckpoint(checkpoint, duration);
    }
    
    public void SetCheckpoint(Checkpoint checkpoint, TimeSpan? duration = null) =>
        _checkpoints.SetCheckpoint(Update.UserId!.Value, checkpoint, duration);
    
    public Checkpoint? GetCheckpoint()
    {
        if (Update.UserId is null)
            throw new InvalidOperationException($"No '{nameof(Update.UserId)}' specified by the context.");

        return _checkpoints.GetCheckpoint(Update.UserId.Value);
    }
    
    public Checkpoint? GetLocalCheckpoint() =>
        _checkpoints.GetLocalCheckpoint(Update.UserId!.Value, Update.ChatId!.Value, HandlerName);
    
    public void ResetCheckpoint()
    {
        if (Update.UserId is null)
            throw new InvalidOperationException($"No '{nameof(Update.UserId)}' specified by the context.");

        _checkpoints.ResetCheckpoint(Update.UserId.Value);
    }
    
    public void SetPreviousReplyMessageId(int messageId) =>
        _replies.SetPreviousReplyMessageId(HandlerName, Update.ChatId!.Value, messageId);

    public int? GetPreviousReplyMessageId() =>
        _replies.TryGetPreviousReplyMessageId(HandlerName, Update.ChatId!.Value);

    public async Task DeletePreviousReplyAsync()
    {
        var messageId = GetPreviousReplyMessageId();
        if (messageId is not null)
            await DeleteMessageAsync(messageId.Value);
    }
    
    public async Task<int> SendTextAsync(
        string text,
        ParseMode? parseMode = null,
        IReplyMarkup? replyMarkup = null)
    {
        var message = await _bot.SendTextMessageAsync(
            Update.ChatId!.Value,
            text,
            parseMode,
            replyMarkup: replyMarkup);
        
        SetPreviousReplyMessageId(message.MessageId);

        return message.MessageId;
    }
    
    public async Task EditTextAsync(
        int messageId,
        string text,
        ParseMode? parseMode = null,
        InlineKeyboardMarkup? replyMarkup = null)
    {
        await _bot.EditMessageTextAsync(new ChatId(Update.ChatId!.Value),
            messageId, text, parseMode, replyMarkup: replyMarkup);
    }

    private async Task DeleteMessageAsync(int messageId) =>
        await _bot.DeleteMessageAsync(Update.ChatId!, messageId);
}
