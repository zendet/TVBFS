using Telegram.Bot;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Models.DTOs;
using TVBFS.Application.Models.Resources;

namespace TVBFS.Application.Models.Contexts;

public class CallbackQueryContext  : ContextBase<ResourcesBase>
{
    public CallbackQueryDto Query = null!;

    public CallbackQueryContext(
        ITelegramBotClient bot,
        ICheckpointMemoryService checkpoints,
        IReplyMemoryService replies)
        : base(bot, checkpoints, replies) { }
}