using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Models.DTOs;
using TVBFS.Application.Models.Resources;

namespace TVBFS.Application.Models.Contexts;

public class CommandContext : ContextBase<CommandResources>
{
    public MessageDto Message = null!;
    public string Payload = null!;

    public CommandContext(
        ITelegramBotClient bot,
        ICheckpointMemoryService checkpoints,
        IReplyMemoryService replies)
        : base(bot, checkpoints, replies) { }
}