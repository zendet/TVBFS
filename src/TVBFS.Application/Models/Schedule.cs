using Newtonsoft.Json;

namespace TVBFS.Application.Models;

public class Classroom
{

    [JsonProperty("name")]
    public string Name { get; set; }

}

public class Day
{
    [JsonProperty("date")]
    public DateTime Date { get; set; }

    [JsonProperty("lessons")]
    public List<DayLesson> Lessons { get; set; }

}

public class DayLesson
{
    [JsonProperty("ring")]
    public byte Ring { get; set; }

    [JsonProperty("startTime")]
    public string StartTime { get; set; }

    [JsonProperty("endTime")]
    public string EndTime { get; set; }

    [JsonProperty("lessons", NullValueHandling = NullValueHandling.Ignore)]
    public List<LessonLesson> Lessons { get; set; }
}

public class LessonLesson
{
    [JsonProperty("studentGroup")]
    public Item StudentGroup { get; set; }

    [JsonProperty("scheduleSubject")]
    public Item ScheduleSubject { get; set; }

    [JsonProperty("classroom")]
    public Classroom Classroom { get; set; }
}

public class Item
{
    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("id")]
    public int Id { get; set; }
}

public class Schedule
{
    [JsonProperty("item")]
    public Item Item { get; set; }

    [JsonProperty("days")]
    public List<Day> Days { get; set; }
}

public class ScheduleSubject
{
    [JsonProperty("name")]
    public string Name { get; set; }
}