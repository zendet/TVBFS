using Newtonsoft.Json;

namespace TVBFS.Application.Models;

public class UserGroupEntries 
{
    [JsonProperty("termNumber")]
    public int TermNumber { get; set; }
    [JsonProperty("name")]
    public string Name { get; set; }
    [JsonProperty("id")]
    public int Id { get; set; }
}