using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Delegates;

public delegate Task BehaviorContextHandler(BehaviorContext context);