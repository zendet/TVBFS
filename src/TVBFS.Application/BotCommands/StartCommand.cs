using MediatR;
using Telegram.Bot.Types.ReplyMarkups;
using TVBFS.Application.Abstractions;
using TVBFS.Application.MediatRCommands;
using TVBFS.Application.MediatRQueries;
using TVBFS.Application.Models.Contexts;
using TVBFS.Application.Services;

namespace TVBFS.Application.BotCommands;

public class StartCommand : IBotCommand, ICallbackQueryHandler
{
    private readonly IMediator _mediator;
    private readonly IHashingService _hasher;

    public StartCommand(
        IMediator mediator,
        IHashingService hasher)
    {
        _mediator = mediator;
        _hasher = hasher;
    }
    
    public async Task ExecuteAsync(CommandContext context, CancellationToken cancellationToken)
    {
        if (await _mediator.Send(new CheckExistingOfUserGroupQuery(
                context.Update.UserId!.Value), cancellationToken))
        {
            ReplyKeyboardMarkup buttons = new(new[]
            {
                new KeyboardButton[]
                {
                    "Получить расписание"
                },
                new KeyboardButton[]
                {
                    "Настройки"
                }
            })
            {
                ResizeKeyboard = true
            };

            await context.SendTextAsync(
                "Вы в главном меню.",
                replyMarkup: buttons);
            
            return;
        }
        
        var response = context.Resources!.Get<string>("SayHello");
        var dbGroupNames = await _mediator.Send(new GetAvailableGroupsNamesListQuery(), cancellationToken);
        
        var factory = new CallbackButtonFactory<StartCommand>
            (_hasher, $"{context.Message.Chat.Id} Group {{0}}");

        var groupButtons = dbGroupNames
            .Select(name =>
                factory.CreateCallbackDataButton(name))
            .ToList();

        var groupsMenu = new List<InlineKeyboardButton[]>();
        for (var i = 0; i < groupButtons.Count; i++)
        {
            if (i + 1 == dbGroupNames.Count)
                groupsMenu.Add(new[] { groupButtons[i] });
            else
            {
                groupsMenu.Add(new[] { groupButtons[i], groupButtons[i + 1] });
                i++;
            }
        }
        
        await context.SendTextAsync(response, replyMarkup: new InlineKeyboardMarkup(groupsMenu));
    }

    public async Task HandleCallbackQueryAsync(CallbackQueryContext context, CancellationToken token)
    {
        SplitCallbackData(context.Query.Data, out _, out _);

        await HandleGroupCallbackAsync(context);
    }

    private async Task HandleGroupCallbackAsync(CallbackQueryContext context)
    {
        await context.EditTextAsync(context.Query.Message.Id,
            "Отлично, вы будете получать расписание занятий для выбранной вами группы.");
        SplitCallbackData(context.Query.Data, out var userId, out var payload);
        
        await _mediator.Send(new AddUserInGroupCommand(userId, payload));
        
        ReplyKeyboardMarkup buttons = new(new[]
        {
            new KeyboardButton[]
            {
                "Получить расписание"
            },
            new KeyboardButton[]
            {
               "Настройки"
            }
        })
        {
            ResizeKeyboard = true
        };

        await context.SendTextAsync(
            "Вы в главном меню.",
            replyMarkup: buttons);
    }

    private static void SplitCallbackData(
        string data,
        out long userId,
        out string payload)
    {
        var words = data.Split();
        userId = long.Parse(words[0]);
        payload = string.Join(' ', words[2..]);
    }
}