using MediatR;
using Telegram.Bot.Types.ReplyMarkups;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Delegates;
using TVBFS.Application.MediatRCommands;
using TVBFS.Application.MediatRHandlers;
using TVBFS.Application.MediatRQueries;
using TVBFS.Application.Models.Contexts;
using TVBFS.Application.Services;

namespace TVBFS.Application.BotCommands;

public class SettingsCommand : IBotCommand, ICallbackQueryHandler
{
    private readonly IHashingService _hasher;
    private readonly IMediator _mediator;
    
    public SettingsCommand(
        IHashingService hasher, 
        IMediator mediator)
    {
        _hasher = hasher;
        _mediator = mediator;
    }
    public async Task ExecuteAsync(CommandContext context, CancellationToken cancellationToken)
    {
        var response = context.Resources!.Get<string>("SendSettings");
        var factory = new CallbackButtonFactory<SettingsCommand>
            (_hasher, $"{context.Message.Chat.Id} Setting {{0}}");

        var subscription = await _mediator.Send(new CheckUserSubscriptionQuery(context.Update.UserId),
            cancellationToken);
        
        var buttons = new List<List<InlineKeyboardButton>>
        {
            new()
            {
                factory.CreateCallbackDataButton("Сменить группу"),
                factory.CreateCallbackDataButton("Кто я?")
            },
            new()
            {
                factory.CreateCallbackDataButton(subscription ? "Отписаться от рассылки" : "Подписаться на рассылку", 
                    subscription ? "Отписаться" : "Подписаться")
            }
        };

        await context.SendTextAsync(response, replyMarkup: new InlineKeyboardMarkup(buttons));
    }

    public async Task HandleCallbackQueryAsync(CallbackQueryContext context, CancellationToken token)
    {
        SplitCallbackData(context.Query.Data, out var userId, out var payload);
        
        CallbackQueryContextHandler callbackQueryHandler = payload.Split().First() switch
        {
            "Отписаться" or "Подписаться" => HandleSubscriptionOrUnsubscriptionAsync, 
            "Кто" => HandleWhoAmIAsync,
            "Сменить" => HandleChangeGroupAsync,
            _ => HandleGroupCallbackAsync
        };

        await callbackQueryHandler(context);
    }

    private async Task HandleChangeGroupAsync(CallbackQueryContext context)
    {
        var dbGroupNames = await _mediator.Send(new GetAvailableGroupsNamesListQuery());
        
        var factory = new CallbackButtonFactory<SettingsCommand>
            (_hasher, $"{context.Update.UserId} ChangingGroup {{0}}");

        var groupButtons = dbGroupNames
            .Select(name =>
                factory.CreateCallbackDataButton(name))
            .ToList();

        var groupsMenu = new List<InlineKeyboardButton[]>();
        for (var i = 0; i < groupButtons.Count; i++)
        {
            if (i + 1 == dbGroupNames.Count)
                groupsMenu.Add(new[] { groupButtons[i] });
            else
            {
                groupsMenu.Add(new[] { groupButtons[i], groupButtons[i + 1] });
                i++;
            }
        }
        
        await context.EditTextAsync(context.Query.Message.Id, "Выберите свою группу:",
            replyMarkup: new InlineKeyboardMarkup(groupsMenu));
    }

    private async Task HandleWhoAmIAsync(CallbackQueryContext context)
    {
        SplitCallbackData(context.Query.Data, out var userId, out _);

        var userGroup = await _mediator.Send(new GetUserGroupQuery(userId));
        var isUserSubscribed = await _mediator.Send(new CheckUserSubscriptionQuery(userId));
        
        await context.EditTextAsync(context.Query.Message.Id, 
            $"Ваша группа: {userGroup}\n" +
            $"Подписаны-ли на автоматическое получение расписания: {(isUserSubscribed ? "Да" : "Нет")}");
    }

    private async Task HandleSubscriptionOrUnsubscriptionAsync(CallbackQueryContext context)
    {
        SplitCallbackData(context.Query.Data, out var userId, out _);

        var subscription = await _mediator.Send(new CheckUserSubscriptionQuery(userId));
        
        await _mediator.Send(new UpdateUserSubscriptionCommand(userId, !subscription));

        await context.EditTextAsync(context.Query.Message.Id,
            subscription
                ? "Вы успешно отписались от автоматического получения расписания!"
                : "Вы успешно подписались на автоматическое получение расписания!");
    }

    private async Task HandleGroupCallbackAsync(CallbackQueryContext context)
    {
        SplitCallbackData(context.Query.Data, out var userId, out var payload);
        
        
        var oldGroupName = await _mediator.Send(new GetUserGroupQuery(context.Update.UserId));
        
        if (payload == oldGroupName)
            await context.EditTextAsync(context.Query.Message.Id, $"Вы и так находитесь в группе {payload}!");
        else
        {
            await _mediator.Send(new AddUserInGroupCommand(userId, payload));

            await context.EditTextAsync(context.Query.Message.Id,
                $"Вы сменили группу с \"{oldGroupName}\" на \"{payload}\".");
        }
    }
    
    private static void SplitCallbackData(
        string data,
        out long userId,
        out string payload)
    {
        var words = data.Split();
        userId = long.Parse(words[0]);
        payload = string.Join(' ', words[2..]);
    }
}