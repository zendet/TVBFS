using Telegram.Bot.Types.ReplyMarkups;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Models.Contexts;
using TVBFS.Application.Services;

namespace TVBFS.Application.BotCommands;

public class GetScheduleCommand : IBotCommand, ICallbackQueryHandler
{
    private readonly IHashingService _hasher;
    private readonly ScheduleService _schedule;

    public GetScheduleCommand(
        IHashingService hasher, 
        ScheduleService schedule)
    {
        _hasher = hasher;
        _schedule = schedule;
    }

    public async Task ExecuteAsync(CommandContext context, CancellationToken cancellationToken)
    {
        var response = context.Resources!.Get("SendSchedule");
        var factory = new CallbackButtonFactory<GetScheduleCommand>
            (_hasher, $"{context.Message.Chat.Id} ScheduleDay {{0}}");

        var buttons = new List<List<InlineKeyboardButton>>
        {
            new()
            {
                factory.CreateCallbackDataButton("Сегодня"),
                factory.CreateCallbackDataButton("Завтра"),
                factory.CreateCallbackDataButton("На неделю")
            },
            new()
            {
                factory.CreateCallbackDataButton("Отмена")
            }
        };

        await context.SendTextAsync(response, replyMarkup: new InlineKeyboardMarkup(buttons));
    }

    public async Task HandleCallbackQueryAsync(CallbackQueryContext context, CancellationToken token)
    {
        SplitCallbackData(context.Query.Data, out var userId, out var payload);

        var response = payload switch
        {
            "Сегодня" or "Завтра" => await _schedule.FormatScheduleOnDayAsync(payload, userId),
            _ => await _schedule.FormatScheduleOnWeekAsync(userId)
        };
        
        await context.EditTextAsync(context.Query.Message.Id,
            response);
    }
    
    private static void SplitCallbackData(
        string data,
        out long userId,
        out string payload)
    {
        var words = data.Split();
        userId = long.Parse(words[0]);
        payload = string.Join(' ', words[2..]);
    }
}