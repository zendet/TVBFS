using MediatR;

namespace TVBFS.Application.MediatRQueries;

public record CheckExistingOfUserGroupQuery(long UserId) : IRequest<bool>;