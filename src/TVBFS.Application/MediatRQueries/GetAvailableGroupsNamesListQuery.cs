using MediatR;

namespace TVBFS.Application.MediatRQueries;

public record GetAvailableGroupsNamesListQuery
    : IRequest<List<string>>;