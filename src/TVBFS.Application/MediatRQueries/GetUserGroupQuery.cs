using MediatR;

namespace TVBFS.Application.MediatRQueries;

public record GetUserGroupQuery(long? UserId) : IRequest<string>;