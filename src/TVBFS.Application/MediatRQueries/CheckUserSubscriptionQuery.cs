using MediatR;

namespace TVBFS.Application.MediatRQueries;

public record CheckUserSubscriptionQuery(long? UserId) : IRequest<bool>;