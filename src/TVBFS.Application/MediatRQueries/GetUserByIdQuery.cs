using MediatR;
using TVBFS.Domain.Models;
using TVBFS.Domain.Models.Entities;

namespace TVBFS.Application.MediatRQueries;

public record GetUserByIdQuery(long UserId) : IRequest<User>;