using MediatR;
using TVBFS.Domain.Models.Entities;

namespace TVBFS.Application.MediatRQueries;

public record GetGroupLessonsListByDateQuery(string Date, long UserId) : IRequest<List<Lesson>>;