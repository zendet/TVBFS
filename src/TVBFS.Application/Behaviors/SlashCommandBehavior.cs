using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Telegram.Bot;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Behaviors;

public class SlashCommandBehavior : CommandBehaviorBase
{
    private readonly ITelegramBotClient _bot;
    private readonly ICommandsService _commands;
    private readonly IResourcesService _resources;
    private readonly ILinguisticParser _parser;

    public SlashCommandBehavior(
        ICommandsService commands,
        IServiceScopeFactory scopeFactory,
        CommandContext commandContext,
        IResourcesService resources, 
        ILinguisticParser parser,
        ITelegramBotClient bot)
        : base(commands,
        scopeFactory,
        commandContext,
        resources,
        parser)
    {
        _commands = commands;
        _resources = resources;
        _parser = parser;
        _bot = bot;
    }

    protected override bool FillCommandContext(CommandContext commandContext, BehaviorContext behaviorContext)
    {
        var slash = commandContext.Message.Text.Split().First();
        if (!slash.StartsWith('/'))
        {
            Log.Information("First word is not a slash");
            return true;
        }

        slash = slash[1..].ToLower();
        
        var commandName = _commands.GetCommandNameBySlash(slash);
        if (commandName is null)
        {
            Log.Information("No command with the given slash found in message");
            return false;
        }

        commandContext.HandlerName = commandName;
        commandContext.Resources = _resources.GetCommandResources(commandContext.HandlerName);
        return true;
    }
}