using Serilog;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Delegates;
using TVBFS.Application.Models;
using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Behaviors;

public class ErrorBehavior : IBehavior
{
    public async Task HandleAsync(BehaviorContext context, BehaviorContextHandler next)
    {
        try
        {
            await next(context);
        }
        catch (Exception e)
        {
            Log.Error(e, "Behavior pipeline has thrown an exception");
            await context.SendTextAsync(context.Resources!.Get("SomethingWentWrong"));
        }
    }
}
