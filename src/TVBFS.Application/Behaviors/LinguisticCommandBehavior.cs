using Microsoft.Extensions.DependencyInjection;
using Serilog;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Behaviors;

public class LinguisticCommandBehavior : CommandBehaviorBase
{
    private readonly ICommandsService _commands;
    private readonly ILinguisticParser _parser;
    private readonly ICheckpointMemoryService _checkpoints;
    private readonly IResourcesService _resources;

    public LinguisticCommandBehavior(
        ICommandsService commands, 
        IServiceScopeFactory scopeFactory, 
        CommandContext commandContext, 
        IResourcesService resources, 
        ILinguisticParser parser, 
        ICheckpointMemoryService checkpoints) 
        : base(commands, scopeFactory, commandContext, resources, parser)
    {
        _commands = commands;
        _resources = resources;
        _parser = parser;
        _checkpoints = checkpoints;
    }

    protected override bool FillCommandContext(CommandContext commandContext, BehaviorContext behaviorContext)
    {
        commandContext.Payload = commandContext.Message.Text;
        
        var checkpoint = _checkpoints.GetCheckpoint(behaviorContext.Update.UserId!.Value);
        if (checkpoint is not null)
        {
            var commandResources = _resources.GetCommandResources(checkpoint.HandlerName);
            if (commandResources is not null)
            {
                commandContext.Resources = _resources.GetCommandResources(checkpoint.HandlerName);
                return true;
            }
        }

        var name = _parser.TryParseName(commandContext.Payload);
        if (name is null)
        {
            Log.Information("No command alias found in message");
            return true;
        }

        commandContext.HandlerName = _commands.GetCommandByName(name.Case)!;
        commandContext.Resources = _resources.GetCommandResources(commandContext.HandlerName);
        return true;
    }
}