using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot.Types.Enums;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Delegates;
using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Behaviors;

public abstract class CommandBehaviorBase : IBehavior
{
    private readonly ICommandsService _commands;
    private readonly IResourcesService _resources;
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly CommandContext _commandContext;
    private readonly ILinguisticParser _parser;
    
    public CommandBehaviorBase(ICommandsService commands,
        IServiceScopeFactory scopeFactory,
        CommandContext commandContext,
        IResourcesService resources, 
        ILinguisticParser parser)
    {
        _commands = commands;
        _scopeFactory = scopeFactory;
        _commandContext = commandContext;
        _resources = resources;
        _parser = parser;
    }
    
    public async Task HandleAsync(BehaviorContext context, BehaviorContextHandler next)
    {
        if (context.Update.Type != UpdateType.Message)
        {
            await next(context);
            return;
        }
        
        var message = context.Update.Message!;
        
        // Fill command context
        _commandContext.Update = context.Update;
        _commandContext.Message = message;
        _commandContext.Payload = message.Text;
        
        // Extract command from checkpoint if possible
        var checkpoint = context.GetCheckpoint();
        if (checkpoint is not null && checkpoint.HandlerName.EndsWith("Command"))
        {
            _commandContext.HandlerName = checkpoint.HandlerName;
            _commandContext.Resources = _resources.GetCommandResources(_commandContext.HandlerName);
        }
        else
        {
            var considerCommand = FillCommandContext(_commandContext, context);
            if (!considerCommand || _commandContext.Resources is null)
            {
                await next(context);
                return;
            }
        }

        using var scope = _scopeFactory.CreateScope();
        var command = _commands.GetCommandInstance(scope, _commandContext.HandlerName);
        await command.ExecuteAsync(_commandContext, new CancellationTokenSource().Token);
    }
    
    protected abstract bool FillCommandContext(CommandContext commandContext, BehaviorContext behaviorContext);
}