using TVBFS.Application.Abstractions;
using TVBFS.Application.Delegates;
using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Behaviors;

public class MisunderstandingBehavior : IBehavior
{
    public async Task HandleAsync(BehaviorContext context, BehaviorContextHandler next)
    {
        try
        {
            await next(context);
        }
        finally
        {
            await context.SendTextAsync(context.Resources!.Get("CommandNotRecognized"));
        }
    }
}