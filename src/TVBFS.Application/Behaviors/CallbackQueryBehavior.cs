using Telegram.Bot.Types.Enums;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Delegates;
using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Behaviors;

public class CallbackQueryBehavior : IBehavior
{
    private readonly CallbackQueryContext _callbackQueryContext;
    private readonly IEnumerable<ICallbackQueryHandler> _callbackQueryHandlers;
    private readonly IHashingService _hasher;

    public CallbackQueryBehavior(
        CallbackQueryContext callbackQueryContext,
        IEnumerable<ICallbackQueryHandler> callbackQueryHandlers,
        IHashingService hasher)
    {
        _callbackQueryContext = callbackQueryContext;
        _callbackQueryHandlers = callbackQueryHandlers;
        _hasher = hasher;
    }
    
    public async Task HandleAsync(BehaviorContext context, BehaviorContextHandler next)
    {
        if (context.Update.Type != UpdateType.CallbackQuery)
        {
            await next(context);
            return;
        }
        
        _callbackQueryContext.Update = context.Update;
        _callbackQueryContext.Query = context.Update.CallbackQuery!;
        _callbackQueryContext.Resources = context.Resources;

        var callbackQueryHandler = _callbackQueryHandlers.First(x =>
            _hasher.Convert(x.GetType().FullName!) == _callbackQueryContext.Query.CallbackHandlerNameHash);

        await callbackQueryHandler.HandleCallbackQueryAsync(_callbackQueryContext,
            new CancellationTokenSource().Token);
    }
}