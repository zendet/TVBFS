using Mapster;
using MediatR;
using Serilog;
using Telegram.Bot.Types;
using TVBFS.Application.Abstractions;
using TVBFS.Application.MediatRCommands;
using TVBFS.Application.Models.Contexts;
using TVBFS.Application.Models.DTOs;

namespace TVBFS.Application.Services;

public class UpdateHandler : IUpdateHandler
{
    private readonly IEnumerable<IBehavior> _behaviors;
    private readonly BehaviorContext _behaviorContext;
    private readonly CallbackQueryContext _callbackQueryContext;
    private readonly IResourcesService _resources;
    private readonly IMediator _mediator;

    public UpdateHandler(
        IEnumerable<IBehavior> behaviors,
        BehaviorContext behaviorContext,
        IResourcesService resources, IMediator mediator,
        CallbackQueryContext callbackQueryContext)
    {
        _behaviors = behaviors;
        _behaviorContext = behaviorContext;
        _resources = resources;
        _mediator = mediator;
        _callbackQueryContext = callbackQueryContext;
    }
    
    public async Task HandleAsync(Update update)
    {
#if DEBUG
        Log.Debug("Received update: {@0}", update);
#else
         Log.Information("Received message ({0}) from chat {1}: {2}",
            update.Message.MessageId, update.Message.Chat.Id, update.Message.Text);
#endif

        FillBehaviorContext(_behaviorContext, update);
        
        await _mediator.Send(new AddOrUpdateUserInDatabaseCommand(_behaviorContext));
        
        var enumerator = _behaviors.GetEnumerator();
        await RunNextBehaviorAsync(_behaviorContext);
        
        async Task RunNextBehaviorAsync(BehaviorContext context)
        {
            var previousHandlerName = context.HandlerName;
            var previousResources = context.Resources;

            if (enumerator.MoveNext())
            {
                context.HandlerName = enumerator.Current.GetType().FullName!;
                context.Resources =
                    _resources.GetBehaviorResources(context.HandlerName);

                Log.Debug("Entering behavior {0}", context.HandlerName);

                try
                {
                    await enumerator.Current.HandleAsync(context, RunNextBehaviorAsync);
                }
                finally
                {
                    Log.Debug("Leaving behavior {0}", context.HandlerName);

                    context.HandlerName = previousHandlerName;
                    context.Resources = previousResources;
                }
            }
        }
    }

    private void FillBehaviorContext(BehaviorContext context, Update update)
    {
        context.Update = update.Adapt<UpdateDto>();
        
        context.Update.CallbackQuery = update.CallbackQuery?.Adapt<CallbackQueryDto>();
    }
}