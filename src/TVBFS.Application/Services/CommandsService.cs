using Microsoft.Extensions.DependencyInjection;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Models;
using TVBFS.Application.Models.Resources;

namespace TVBFS.Application.Services;

public class CommandsService : ICommandsService
{
    private readonly IDictionary<string, CommandResources> _commandResources;

    public CommandsService(IDictionary<string, CommandResources> commandResources) =>
        _commandResources = commandResources;

    public IBotCommand GetCommandInstance(IServiceScope scope, string commandName)
    {
        var commandType = Type.GetType(commandName)!;
        return (IBotCommand)scope.ServiceProvider.GetRequiredService(commandType);
    }
    
    public string? GetCommandByName(string name)
    {
        var commandNames = _commandResources.Select(x => new
        {
            CommandName = x.Key, 
            Names = x.Value.Names
        }).ToArray();
            
        var commandName = commandNames.FirstOrDefault(x => x.Names.Contains(name));
        return commandName?.CommandName;
    }

    public string? GetCommandNameBySlash(string slash)
    {
        var commandSlashes = _commandResources.Select(x => new
        {
           CommandName = x.Key, x.Value.Slashes
        }).ToArray();
            
        var commandSlash = commandSlashes.FirstOrDefault(x => x.Slashes.Contains(slash));
        return commandSlash?.CommandName;
    }
}