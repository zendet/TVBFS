using Microsoft.Extensions.Caching.Memory;
using TVBFS.Application.Abstractions;

namespace TVBFS.Application.Services;

public class ReplyMemoryService : IReplyMemoryService
{
    private const string PreviousReplyEntryNameTemplate = "__Reply__Previous__{0}__{1}";
    private static readonly TimeSpan MemoryDuration = TimeSpan.FromHours(1);
    private readonly IMemoryCache _cache;

    public ReplyMemoryService(IMemoryCache cache)
    {
        _cache = cache;
    }

    public void SetPreviousReplyMessageId(string handlerName, long chatId, int messageId) => 
        _cache.Set(GetPreviousReplyEntryName(handlerName, chatId), messageId, MemoryDuration);

    public int? TryGetPreviousReplyMessageId(string handlerName, long chatId) =>
        _cache.TryGetValue(GetPreviousReplyEntryName(handlerName, chatId), out int messageId)
            ? messageId
            : null;
    
    private static string GetPreviousReplyEntryName(string handlerName, long chatId) =>
        string.Format(PreviousReplyEntryNameTemplate, handlerName, chatId);
}