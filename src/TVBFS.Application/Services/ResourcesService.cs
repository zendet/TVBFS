using TVBFS.Application.Abstractions;
using TVBFS.Application.Models;
using TVBFS.Application.Models.Resources;

namespace TVBFS.Application.Services;

public class ResourcesService : IResourcesService
{
    private readonly IDictionary<string, BehaviorResources> _behaviorResources;
    private readonly IDictionary<string, CommandResources> _commandResources;
    // private readonly IDictionary<string, CallbackResources> _callbackResources;

    public ResourcesService(
        IDictionary<string, BehaviorResources> behaviorResources,
        IDictionary<string, CommandResources> commandResources)
        // IDictionary<string, CallbackResources> callbackResources)
    {
        _behaviorResources = behaviorResources;
        _commandResources = commandResources;
        // _callbackResources = callbackResources;
    }

    public CommandResources? GetCommandResources<TCommand>() where TCommand : IBotCommand =>
        GetCommandResources(typeof(TCommand).FullName!);

    public CommandResources? GetCommandResources(string commandName) => 
        _commandResources.TryGetValue(commandName, out var resources) ? resources : null;

    public BehaviorResources? GetBehaviorResources(string behaviorName)
    {
        _behaviorResources.TryGetValue(behaviorName, out var resources);
        return resources;
    }

    // public CallbackResources? GetCallbackResources(string callbackName)
    // {
    //     _callbackResources.TryGetValue(callbackName, out var resources);
    //     return resources;
    // }
}