using TVBFS.Application.Abstractions;
using TVBFS.Application.Models;
using TVBFS.Application.Models.Resources;

namespace TVBFS.Application.Services;

public class LinguisticParser : ILinguisticParser
{
    private readonly IDictionary<string, CommandResources> _commandResources;

    public LinguisticParser(
        IDictionary<string, CommandResources> commandResources)
    {
        _commandResources = commandResources;
    }

    public CaseParsingResult? TryParseName(string text)
    {
        var names = _commandResources.Values.SelectMany(x => x.Names);
        var name = names.FirstOrDefault(name => name == text);
        return new CaseParsingResult(name);
    }
}