using Telegram.Bot.Types.ReplyMarkups;
using TVBFS.Application.Abstractions;

namespace TVBFS.Application.Services;

public class CallbackButtonFactory<THandler> : ICallbackButtonFactory
    where THandler : ICallbackQueryHandler
{
    private readonly string _callbackHandlerNameHash;
    private readonly string _dataFormat;
    
    public CallbackButtonFactory(IHashingService hasher, string? dataFormat = null)
    {
        _callbackHandlerNameHash = hasher.Convert(typeof(THandler).FullName!);
        _dataFormat = dataFormat ?? "{0}";
    }
    public InlineKeyboardButton CreateCallbackDataButton(string text, string? data = null) =>
        InlineKeyboardButton.WithCallbackData(text, DecorateWithCallbackDataHeaders(data ?? text));
    
    private string DecorateWithCallbackDataHeaders(string data) =>
        $"{_callbackHandlerNameHash}{string.Format(_dataFormat, data)}";
}