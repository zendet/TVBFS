using System.Net;
using System.Net.Http.Headers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using TVBFS.Application.Abstractions;
using TVBFS.Application.MediatRQueries;
using TVBFS.Application.Models;
using TVBFS.Domain.Models.Entities;
using TVBFS.Persistence;

namespace TVBFS.Application.Services;

public class ScheduleService // : IScheduleService
{
    // private static readonly HttpClient _client = new();
    private readonly string _login;
    private readonly string _password;
    
    private readonly string _loginUrl;
    private readonly string _allGroupsUrl;
    private readonly string _plainDomain;
        
    private CookieCollection _collectionOfCookies = null!;

    private readonly ScheduleDbContext _dbContext;
    private readonly IBase64ConverterService _converter;
    private readonly IMediator _mediator;

    public ScheduleService(
        ScheduleDbContext dbContext,
        IBase64ConverterService converter, 
        IConfiguration configuration, 
        IMediator mediator)
    {
        _dbContext = dbContext;
        _converter = converter;
        _mediator = mediator;
        
        _plainDomain = configuration["ScheduleUrls:PlainDomain"]!;

        _login = configuration["ScheduleConfiguration:Login"]!;
        _password = configuration["ScheduleConfiguration:Password"]!;

        _loginUrl = configuration["ScheduleUrls:LoginUrl"]!;
        _allGroupsUrl = configuration["ScheduleUrls:AllGroupsUrl"]!;
    }
        
    public Schedule GetCurrentSchedule(string name)
    {
        _collectionOfCookies = GetCookieFromDb();
        return JsonConvert.DeserializeObject<Schedule>
            (GetJson(GetScheduleFromCurrentWeekToNextSaturday(name)));
    }
    
    public Schedule GetScheduleOnNextWeek(string name)
    {
        _collectionOfCookies = GetCookieFromDb();
        return JsonConvert.DeserializeObject<Schedule>
            (GetJson(GetScheduleFromTo(GetNextWeekday(DateTime.Today, DayOfWeek.Monday),
                GetNextWeekday(DateTime.Today, DayOfWeek.Saturday), name)));
    }

    public async Task<string> FormatScheduleOnDayAsync(string date, long chatId) 
    {
        date = date switch
        {
            "Сегодня" => DateTime.Today.ToString("MM/dd/yyyy"),
            "Завтра" => DateTime.Today.AddDays(1).ToString("MM/dd/yyyy"),
            _ => date + $"/{DateTime.Today.Year}"
        };

        var lessons = await _mediator.Send(new GetGroupLessonsListByDateQuery(date, chatId));

        if (!lessons.Any())
            return "";

        var message = $"Дата: {lessons[0].Date.AddDays(1):yyyy-MM-dd}.\n";
        foreach (var lesson in lessons)
        {
            message += $"  {lesson.OrdinalLessonNumber}-я пара [{lesson.StartTime}-{lesson.EndTime}]: " +
                       $"{lesson.Title}, ";
            if (!char.IsDigit(lesson.Classroom[0]))
                message += $"{lesson.Classroom}\n";
            else
                message += $"кабинет №{lesson.Classroom}\n";
        }

        return message + "\n";
    }

    public async Task<string> FormatScheduleOnWeekAsync(long chatId)
    {
        var schedule = string.Empty;
        
        for (var i = 0; i < 6; i++)
        {
            if (DateTime.Today.AddDays(i).DayOfWeek is not DayOfWeek.Sunday)
                schedule += await FormatScheduleOnDayAsync(DateTime.Today.AddDays(i).ToString("MM/dd"), chatId);
        }

        return schedule;
    }

    private CookieCollection GetCookieThroughLogin()
    {
        var cookieContainer = new CookieContainer();
        using HttpClientHandler httpClientHandler = new()
        {
            CookieContainer = cookieContainer
        };
        using HttpClient httpClient = new(httpClientHandler);
        using HttpRequestMessage request =
            new(new HttpMethod("POST"), _loginUrl);
            
        var hex = _converter.Convert(_password);
            
        request.Content = 
            new StringContent($"{{\"login\":\"{_login}\", \"password\":\"{hex}\"}}");
        request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

        httpClient.Send(request);
            
        return cookieContainer.GetAllCookies();
    }

    private CookieCollection GetCookieFromDb() 
    {
        CookieCollection cookieCollection = new();
        if (!_dbContext.CachedCookies.Any())
        {
            var cookies = GetCookieThroughLogin();
            for (var i = 0; i < cookies.Count; i++)
            {
                var cookie = new CachedCookie
                {
                    Name = cookies[i].Name,
                    Value = cookies[i].Value,
                    Domain = cookies[i].Domain,
                    IsDbCookiesActual = true
                };
                _dbContext.CachedCookies.Add(cookie);
            }

            _dbContext.SaveChanges();
        }

        foreach (var cookies in _dbContext.CachedCookies.AsNoTracking())
            cookieCollection.Add(new Cookie(cookies.Name, cookies.Value, "/", cookies.Domain));

        return cookieCollection;
    }

    private string GetJson(string uri)
    {
        var webRequest = (HttpWebRequest)WebRequest.Create(uri); // Заменить на HttpClient
        webRequest.AllowAutoRedirect = false;
        webRequest.CookieContainer = new CookieContainer();

        for (var i = 0; i < _collectionOfCookies.Count; i++)
            webRequest.CookieContainer.Add(_collectionOfCookies[i]);

        try
        {
            using StreamReader streamReader = new(webRequest.GetResponse().GetResponseStream());
            return streamReader.ReadToEnd();
        }
        catch (WebException exception)
        {
            Console.WriteLine(exception.Message); // Придумать лучшее решение
            throw;
        }
    }

    private int GetStudentGroupId(string name)
    {   
        var deserializedStudentGroups = JsonConvert.DeserializeObject<List<UserGroup>>(
            GetJson(_allGroupsUrl));

        return deserializedStudentGroups.Find(group => group.Title.ToUpper().StartsWith(name)).Id;
    }

    private int GetStudentGroupIdAccordingToSemester(string name)
    {
        var studentGroupEntries = JsonConvert.DeserializeObject<List<UserGroupEntries>>(GetJson(
            GetEntryUrl(name)));

        if (DateTime.Now.Month >= 9)
            return studentGroupEntries.Find(group =>
                group.Name.ToUpper().StartsWith(name) && group.TermNumber == 1).Id;

        return studentGroupEntries.Find(group =>
            group.Name.ToUpper().StartsWith(name) && group.TermNumber == 2).Id;
    }

    private string GetEntryUrl(string name) => 
        new($"{_plainDomain}/services/schedule/timetable/{GetStudentGroupId(name)}/entries");

    public List<UserGroup>? GetAllGroupsNames()
    {
        _collectionOfCookies = GetCookieFromDb();
        return JsonConvert.DeserializeObject<List<UserGroup>>(
            GetJson(_allGroupsUrl));
    }

    private string GetScheduleFromCurrentWeekToNextSaturday(string name)
    {
        return GetScheduleFromTo(DateTime.Today, GetNextWeekday(DateTime.Today, DayOfWeek.Saturday), name);
    }

    private static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
    {
        if (DateTime.Today.DayOfWeek == day)
            return start.AddDays(7);

        // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
        var daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
        return start.AddDays(daysToAdd);
    }
    
    private string GetScheduleFromTo(DateTime startTime, DateTime endTime, string name)
    {
        return $"{_plainDomain}/services/schedule/timetable/{startTime.Year}" +
               $"-{startTime.Month}-{startTime.Day}/{endTime.Year}" +
               $"-{endTime.Month}-{endTime.Day}?id=" +
               $"{GetStudentGroupIdAccordingToSemester(name)}&type=studentGroup";

    }
}