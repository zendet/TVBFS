using System.Security.Cryptography;
using System.Text;
using TVBFS.Application.Abstractions;

namespace TVBFS.Application.Services;

public class Base64ConverterService : IBase64ConverterService
{
    public string Convert(string text) => ToBase64(ToHex(text));
    
    private string ToHex(string text)
    {
        using var hash = SHA256.Create();

        var bytes = hash.ComputeHash(Encoding.UTF8.GetBytes(text));

        var hex = new StringBuilder();
        foreach (var t in bytes)
        {
            hex.Append(t.ToString("x2"));
        }

        return hex.ToString();
    }

    private string ToBase64(string hex)
    {
        var resultantArray = new byte[hex.Length / 2];
        for (var i = 0; i < resultantArray.Length; i++)
            resultantArray[i] = System.Convert.ToByte(hex.Substring(i * 2, 2), 16); // here going kinda magic

        return System.Convert.ToBase64String(resultantArray);
    }

}