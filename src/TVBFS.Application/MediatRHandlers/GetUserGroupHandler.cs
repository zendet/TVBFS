using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRQueries;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class GetUserGroupHandler : IRequestHandler<GetUserGroupQuery, string>
{
    private readonly ScheduleDbContext _dbContext;

    public GetUserGroupHandler(
        ScheduleDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<string> Handle(GetUserGroupQuery request, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.AsNoTracking().Include(user => user.Group)
            .FirstAsync(u => u.TelegramUser!.TelegramId == request.UserId,
            cancellationToken);

        return user.Group!.Title;
    }
}