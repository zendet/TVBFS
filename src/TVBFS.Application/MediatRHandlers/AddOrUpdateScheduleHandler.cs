using System.Text.RegularExpressions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRCommands;
using TVBFS.Application.Services;
using TVBFS.Domain.Models.Entities;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class AddOrUpdateScheduleHandler : IRequestHandler<AddOrUpdateScheduleCommand, List<User>>
{
    private readonly Regex _titlePattern = new(@"/\w.+$");
    
    private readonly ScheduleService _schedule;
    private readonly ScheduleDbContext _dbContext;

    public AddOrUpdateScheduleHandler(
        ScheduleService schedule,
        ScheduleDbContext dbContext)
    {
        _schedule = schedule;
        _dbContext = dbContext;
    }
    
    public async Task<List<User>> Handle(AddOrUpdateScheduleCommand request, CancellationToken cancellationToken)
    {
        var users = new List<User>();

        if (request.UpdateSchedule)
            await _dbContext.Database.ExecuteSqlRawAsync("TRUNCATE TABLE \"Lessons\" RESTART IDENTITY CASCADE",
                cancellationToken);
        
        foreach (var name in request.DbGroupNames)
        {
            var deserializedSchedule = request.UpdateSchedule
                ? _schedule.GetScheduleOnNextWeek(name)
                : _schedule.GetCurrentSchedule(name);

            if (deserializedSchedule.Days.FirstOrDefault()?.Date ==
                _dbContext.Lessons.FirstOrDefault(lesson => lesson.Title == name)?.Date)
                continue;

            foreach (var day in deserializedSchedule.Days)
            {
                foreach (var lesson in day.Lessons)
                {
                    if (lesson?.Lessons == null) continue;
                    foreach (var subGroupLesson in lesson.Lessons) // possible subgroups in lessons that can be skipped
                    {
                        var less = new Lesson
                        {
                            Date = day.Date.ToUniversalTime(),
                            Group = await _dbContext.Groups.FirstAsync(g => g.Title == name, cancellationToken),
                            StartTime = lesson.StartTime ?? "??:??", // startTime can be null
                            EndTime = lesson.EndTime ?? "??:??", // endTime can be null too (wtf)
                            Classroom = subGroupLesson.Classroom.Name,
                            OrdinalLessonNumber = lesson.Ring,
                            Title = _titlePattern.IsMatch(subGroupLesson.ScheduleSubject.Name)
                                ? _titlePattern.Split(subGroupLesson.ScheduleSubject.Name).First() // if title have / symbol - cut to end
                                : subGroupLesson.ScheduleSubject.Name,
                        };

                        await _dbContext.Lessons.AddAsync(less, cancellationToken);
                        await _dbContext.SaveChangesAsync(cancellationToken);

                        if (lesson.Lessons.Count > 1)
                            break; // necessary for skipping stupid subgroups in lessons 
                    }
                }
            }

            if (deserializedSchedule.Days.Count != 0 && request.UpdateSchedule)
            {
                var usersToBeNotified = await _dbContext.Users
                    .Include(user => user.Group)
                    .Include(user => user.TelegramUser)
                    .Where(user => user.Group!.Title == name && user.IsAutoScheduleSubscriber)
                    .ToListAsync(cancellationToken);

                users.AddRange(usersToBeNotified);
            }
        }

        return users;
    }
}