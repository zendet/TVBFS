using MediatR;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot.Types.ReplyMarkups;
using TVBFS.Application.Abstractions;
using TVBFS.Application.BotCommands;
using TVBFS.Application.MediatRQueries;
using TVBFS.Application.Services;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class GetAvailableGroupsNamesListHandler
    : IRequestHandler<GetAvailableGroupsNamesListQuery, List<string>>
{
    private readonly ScheduleDbContext _dbContext;
    private readonly IHashingService _hasher;

    public GetAvailableGroupsNamesListHandler(
        ScheduleDbContext dbContext, 
        IHashingService hasher)
    {
        _dbContext = dbContext;
        _hasher = hasher;
    }
    
    public async Task<List<string>> Handle(GetAvailableGroupsNamesListQuery request, CancellationToken cancellationToken)
    {
        return await _dbContext.Groups.AsNoTracking()
            .Select(g => g.Title)
            .ToListAsync(cancellationToken: cancellationToken);
    }
}