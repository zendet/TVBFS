using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRCommands;
using TVBFS.Domain.Models.Entities;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class UpdateUserSubscriptionHandler : IRequestHandler<UpdateUserSubscriptionCommand, User>
{
    private readonly ScheduleDbContext _dbContext;

    public UpdateUserSubscriptionHandler(
        ScheduleDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<User> Handle(UpdateUserSubscriptionCommand request, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
            .FirstAsync(u => u.TelegramUser!.TelegramId == request.UserId,
            cancellationToken);

        user.IsAutoScheduleSubscriber = request.Subscription;
        _dbContext.Update(user);
        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return user;
    }
}