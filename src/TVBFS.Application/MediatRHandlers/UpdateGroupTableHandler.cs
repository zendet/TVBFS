using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRCommands;
using TVBFS.Application.Models;
using TVBFS.Domain.Models;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class UpdateGroupTableHandler : IRequestHandler<UpdateGroupTableCommand, List<UserGroup>>
{
    private readonly ScheduleDbContext _dbContext;

    public UpdateGroupTableHandler(ScheduleDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<List<UserGroup>> Handle(UpdateGroupTableCommand request, CancellationToken cancellationToken)
    {
        await _dbContext.Database.ExecuteSqlRawAsync("TRUNCATE TABLE \"Groups\" RESTART IDENTITY CASCADE",
            cancellationToken);

        foreach (var group in request.UserGroups)
        {
            await _dbContext.Groups.AddAsync(new Group()
            {
                Title = group.Title[..4]
            }, cancellationToken);
        }

        await _dbContext.SaveChangesAsync(cancellationToken);
        return request.UserGroups;
    }
}