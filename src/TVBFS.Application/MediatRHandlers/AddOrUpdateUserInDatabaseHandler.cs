using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRCommands;
using TVBFS.Domain.Models;
using TVBFS.Domain.Models.Entities;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class AddOrUpdateUserInDatabaseHandler : IRequestHandler<AddOrUpdateUserInDatabaseCommand, User>
{
    private readonly ScheduleDbContext _dbContext;
    
    public AddOrUpdateUserInDatabaseHandler(ScheduleDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<User> Handle(AddOrUpdateUserInDatabaseCommand request, CancellationToken cancellationToken)
    {
        User user;
        if (await _dbContext.Users.FirstOrDefaultAsync(user =>
                    user.TelegramUser.TelegramId == request.BehaviorContext.Update.ChatId,
                cancellationToken: cancellationToken) == null)
        {
            TelegramUser tgUser = new()
            {
                TelegramId = request.BehaviorContext.Update.ChatId,
                TelegramUsername = request.BehaviorContext.Update.Message?.Sender.Username ??
                                   request.BehaviorContext.Update.CallbackQuery?.Sender.Username,
                TelegramFirstName = request.BehaviorContext.Update.Message?.Sender.FirstName ??
                                    request.BehaviorContext.Update.CallbackQuery?.Sender.FirstName,
                TelegramLastName = request.BehaviorContext.Update.Message?.Sender.LastName ??
                                   request.BehaviorContext.Update.CallbackQuery?.Sender.LastName
            };

            user = new User
            {
                IsAutoScheduleSubscriber = false,
                TelegramUser = tgUser,
                FirstSeen = DateTime.Now.ToUniversalTime(),
                LastSeen = DateTime.Now.ToUniversalTime(),
            };
            await _dbContext.Users.AddAsync(user, cancellationToken);
        }
        else
        {
            user = await _dbContext.Users.FirstAsync(u =>
                u.TelegramUser!.TelegramId == request.BehaviorContext.Update.ChatId, cancellationToken);
            user.LastSeen = DateTime.Now.ToUniversalTime();

            _dbContext.Update(user);
        }
        
        await _dbContext.SaveChangesAsync(cancellationToken);

        return user;
    }
}