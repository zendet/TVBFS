using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRQueries;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class CheckExistingOfUserGroupHandler : IRequestHandler<CheckExistingOfUserGroupQuery, bool>
{
    private readonly ScheduleDbContext _dbContext;

    public CheckExistingOfUserGroupHandler(
        ScheduleDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<bool> Handle(CheckExistingOfUserGroupQuery request, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.Include(u => u.Group)
            .FirstAsync(u => u.TelegramUser!.TelegramId == request.UserId,
            cancellationToken: cancellationToken);
        
        return user.Group != null;
    }
}