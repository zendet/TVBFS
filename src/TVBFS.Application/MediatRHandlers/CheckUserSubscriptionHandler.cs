using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRQueries;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class CheckUserSubscriptionHandler : IRequestHandler<CheckUserSubscriptionQuery, bool>
{
    private readonly ScheduleDbContext _dbContext;

    public CheckUserSubscriptionHandler(
        ScheduleDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<bool> Handle(CheckUserSubscriptionQuery request, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.AsNoTracking()
            .FirstAsync(u => u.TelegramUser!.TelegramId == request.UserId,
            cancellationToken: cancellationToken);
        
        return user.IsAutoScheduleSubscriber;
    }
}