using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRQueries;
using TVBFS.Domain.Models.Entities;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class GetGroupLessonsListByDateHandler : IRequestHandler<GetGroupLessonsListByDateQuery, List<Lesson>>
{
    private readonly ScheduleDbContext _dbContext;

    public GetGroupLessonsListByDateHandler(
        ScheduleDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<List<Lesson>> Handle(GetGroupLessonsListByDateQuery request, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.AsNoTracking()
            .Include(user => user.Group)
            .FirstAsync(user => user.TelegramUser!.TelegramId == request.UserId, cancellationToken);
        
        return await _dbContext.Lessons.AsNoTracking()
            .Where(lesson => lesson.Date == DateTime.Parse(request.Date)
                                 .ToUniversalTime() && lesson.Group == user.Group)
            .OrderBy(lesson => lesson.OrdinalLessonNumber).ToListAsync(cancellationToken);
    }
}