using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRQueries;
using TVBFS.Domain.Models;
using TVBFS.Domain.Models.Entities;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class GetUserByIdHandler : IRequestHandler<GetUserByIdQuery, User>
{
    private readonly ScheduleDbContext _dbContext;

    public GetUserByIdHandler(ScheduleDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<User> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
    {
        return await _dbContext.Users.FirstAsync(user =>
            user.TelegramUser!.TelegramId == request.UserId, cancellationToken);
    }
}