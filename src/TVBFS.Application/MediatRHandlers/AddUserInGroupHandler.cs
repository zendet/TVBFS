using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRCommands;
using TVBFS.Domain.Models;
using TVBFS.Domain.Models.Entities;
using TVBFS.Persistence;

namespace TVBFS.Application.MediatRHandlers;

public class AddUserInGroupHandler : IRequestHandler<AddUserInGroupCommand, User>
{
    private readonly ScheduleDbContext _dbContext;

    public AddUserInGroupHandler(
        ScheduleDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<User> Handle(AddUserInGroupCommand request, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.FirstAsync(u => u.TelegramUser!.TelegramId == request.UserId,
            cancellationToken: cancellationToken);
        
        user.Group = await _dbContext.Groups.FirstAsync(g => g.Title == request.GroupName, 
            cancellationToken: cancellationToken);
        user.IsAutoScheduleSubscriber = true;
        
        _dbContext.Update(user);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return user;
    }
}