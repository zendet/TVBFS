using MediatR;
using Microsoft.EntityFrameworkCore;
using TVBFS.Application.MediatRCommands;
using TVBFS.Application.MediatRQueries;
using TVBFS.Persistence;

namespace TVBFS.Application.Jobs;

public class FetchScheduleJob
{
    private readonly IMediator _mediator;
    private readonly ScheduleDbContext _dbContext;

    public FetchScheduleJob(
        IMediator mediator, 
        ScheduleDbContext dbContext)
    {
        _mediator = mediator;
        _dbContext = dbContext;
    }
    
    public async Task Execute()
    {
        if (await _dbContext.Lessons.CountAsync() != 0)
           return;
 
        var dbGroupNames = await _mediator.Send(new GetAvailableGroupsNamesListQuery());
        await _mediator.Send(new AddOrUpdateScheduleCommand(dbGroupNames, false));
    }
}