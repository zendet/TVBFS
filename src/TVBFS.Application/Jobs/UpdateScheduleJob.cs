using MediatR;
using Telegram.Bot;
using TVBFS.Application.MediatRCommands;
using TVBFS.Application.MediatRQueries;
using TVBFS.Application.Models.Contexts;

namespace TVBFS.Application.Jobs;

public class UpdateScheduleJob
{
    private readonly IMediator _mediator;
    private readonly ITelegramBotClient _botClient;

    public UpdateScheduleJob(
        IMediator mediator,
        ITelegramBotClient botClient)
    {
        _mediator = mediator;
        _botClient = botClient;
    }

    public async Task Execute()
    {
        var dbGroupNames = await _mediator.Send(new GetAvailableGroupsNamesListQuery());
        var users = await _mediator.Send(new AddOrUpdateScheduleCommand(dbGroupNames, true));

        if (users.Count == 0)
            return;
        
        foreach (var user in users)
            await _botClient.SendTextMessageAsync(user.TelegramUser!.TelegramId!, "Для вашей группы появилось новое " +
                "расписание!\nНажмите \"Получить расписание\" для получения актуального расписания.");
        
    }
}