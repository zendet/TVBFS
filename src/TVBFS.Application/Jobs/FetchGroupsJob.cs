using MediatR;
using TVBFS.Application.MediatRCommands;
using TVBFS.Application.MediatRQueries;
using TVBFS.Application.Models;
using TVBFS.Application.Services;

namespace TVBFS.Application.Jobs;

public class FetchGroupsJob
{
    private readonly ScheduleService _schedule;
    private readonly IMediator _mediator;

    public FetchGroupsJob(
        ScheduleService schedule,
        IMediator mediator)
    {
        _schedule = schedule;
        _mediator = mediator;
    }
    
    public async Task Execute()
    {
        var fetchedGroupNames = _schedule.GetAllGroupsNames();
        
        var dbGroupNames = await _mediator.Send(new GetAvailableGroupsNamesListQuery());
        
        if (fetchedGroupNames.Count > dbGroupNames.Count // todo: i should compare hashes
            || fetchedGroupNames.Count < dbGroupNames.Count
            || !CheckGroupNameMatching(fetchedGroupNames, dbGroupNames)) // if one of the group (names) of fetchedGroupNames isn't match dbGroupNames
            await _mediator.Send(new UpdateGroupTableCommand(fetchedGroupNames));
    }

    private bool CheckGroupNameMatching(IReadOnlyList<UserGroup> fetchedGroupNames, IReadOnlyList<string> dbGroupNames)
    {
        for (var i = 0; i < dbGroupNames.Count; i++)
            if (fetchedGroupNames[i].Title[..4] != dbGroupNames[i]) return false;
        
        return true;
    }
}