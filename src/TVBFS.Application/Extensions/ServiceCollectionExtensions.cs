using System.Reflection;
using Hangfire;
using Hangfire.PostgreSql;
using Mapster;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using Serilog;
using TVBFS.Application.Abstractions;
using TVBFS.Application.Behaviors;
using TVBFS.Application.Services;
using MediatR;
using TVBFS.Application.Jobs;
using TVBFS.Application.Models.Contexts;
using TVBFS.Application.Models.DTOs;
using TVBFS.Application.Models.Resources;

namespace TVBFS.Application.Extensions;

public static class ServiceCollectionExtensions
{
    private const string CommandsPath = "Resources/Commands/{0}.json";
    private const string BehaviorsPath = "Resources/Behaviors/{0}.json";
    private const string CallbacksPath = "Resources/Callbacks/{0}.json";

    public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration) =>
        services
            .AddMemoryCache()
            .AddMapsterConfiguration(configuration)
            .AddCommands()
            .AddHangfire(configuration)
            .AddCallbackQueryHandlers()
            .AddBehaviors()
            .AddMediatR()
            .AddLinguisticParsing()
            .AddTransient<FetchGroupsJob>()
            .AddScoped<IUpdateHandler, UpdateHandler>()
            .AddScoped<IResourcesService, ResourcesService>()
            .AddTransient<ScheduleService>()
            .AddSingleton<ICheckpointMemoryService, CheckpointMemoryService>()
            .AddSingleton<ICommandsService, CommandsService>()
            .AddSingleton<IBase64ConverterService, Base64ConverterService>()
            .AddSingleton<IHashingService, Md5HashingService>()
            .AddSingleton<IReplyMemoryService, ReplyMemoryService>()
            .AddScoped<BehaviorContext>()
            .AddScoped<IStateService, StateService>()
            .AddScoped<CommandContext>()
            .AddScoped<CallbackQueryContext>();

    private static IServiceCollection AddHangfire(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddHangfire(options =>
            options.UsePostgreSqlStorage(configuration.GetConnectionString("HangfireConnection")));
        
        return services.AddHangfireServer();
    }
    
    private static IServiceCollection AddLinguisticParsing(this IServiceCollection services) => services
        .AddSingleton<ILinguisticParser, LinguisticParser>();
    
    private static IServiceCollection AddMapsterConfiguration(this IServiceCollection services,
        IConfiguration configuration)
    {
        var keyLength = configuration.GetValue<int>("Callbacks:CallbackHandlerKeyLength");
        
        TypeAdapterConfig<Telegram.Bot.Types.Update, UpdateDto>.NewConfig()
            .Map(
                destination => destination.CallbackQuery,
                source => source.CallbackQuery == null
                    ? null
                    : source.CallbackQuery.Adapt<CallbackQueryDto>());
        
        TypeAdapterConfig<Telegram.Bot.Types.CallbackQuery, CallbackQueryDto>.NewConfig()
            .Map(
                destination => destination.CallbackHandlerNameHash,
                source => source.Data == null
                    ? null
                    : source.Data.Substring(0, keyLength))
            .Map(
                destination => destination.Data,
                source => source.Data == null
                    ? null
                    : source.Data.Substring(keyLength))
            .Map(
                destination => destination.Sender,
                source => source.From.Adapt<UserDto>());

        TypeAdapterConfig<Telegram.Bot.Types.Message, MessageDto>.NewConfig()
            .Map(
                destination => destination.Id,
                source => source.MessageId)
            .Map(
                destination => destination.Sender,
                source => source.From == null
                    ? null
                    : source.From.Adapt<UserDto>());
    
        return services;
    }
    private static IServiceCollection AddCommands(this IServiceCollection services)
    {
        var commandTypes = GetImplementationsOf<IBotCommand>().ToArray();

        foreach (var commandType in commandTypes)
        {
            Log.Information("Registering command \"{0}\"", commandType.FullName);
            services.AddScoped(commandType);
        }

        return services.AddCommand(commandTypes.Select(x => x.FullName!));
    }
    
    private static IServiceCollection AddCommand(
        this IServiceCollection services, IEnumerable<string> commandNames)
    {
        var resourceMap = commandNames.ToDictionary(
            commandTypeName => commandTypeName, commandTypeName =>
            {
                var path = string.Format(
                    CommandsPath, commandTypeName.Split('.').Last());

                var data = ParseJObjectFromRelativeLocation(path);
                return data is not null
                    ? new CommandResources(data)
                    : null;
            });

        return services.AddSingleton<IDictionary<string, CommandResources>>(resourceMap!);
    }

    private static IServiceCollection AddCallbackQueryHandlers(this IServiceCollection services)
    {
        var handlers = GetImplementationsOf<ICallbackQueryHandler>().ToArray();
        foreach (var handler in handlers)
        {
            Log.Information("Registering callback query handler of type \"{0}\"", handler.FullName);
            services.AddScoped(typeof(ICallbackQueryHandler), handler);
        }

        return services;
    }

    private static IServiceCollection AddBehaviors(this IServiceCollection services)
    {
        var implementations = new[]
        {
            typeof(ErrorBehavior),
            typeof(CallbackQueryBehavior),
            typeof(SlashCommandBehavior),
            typeof(LinguisticCommandBehavior),
            typeof(MisunderstandingBehavior),
            typeof(NotEnoughRightsBehavior)
        };

        foreach (var implementation in implementations)
        {
            services.AddScoped(typeof(IBehavior), implementation);
            services.AddScoped(implementation, implementation);
        }

        var behaviorTypes = GetImplementationsOf<IBehavior>();
        foreach (var behaviorType in behaviorTypes)
        {
            if (!behaviorType.IsAbstract && services.All(x => x.ImplementationType != behaviorType))
                Log.Warning("Behavior of type \"{0}\" is not registered", behaviorType.FullName);
        }

        return services.AddBehavior(implementations.Select(x => x.FullName!)); 
    }

    private static IServiceCollection AddBehavior(
        this IServiceCollection services, IEnumerable<string> behaviorNames)
    {
        var resourceMap = behaviorNames.ToDictionary(
            behaviorName => behaviorName, behaviorName =>
            {
                var path = string.Format(
                    BehaviorsPath, behaviorName.Split('.').Last());

                var data = ParseJObjectFromRelativeLocation(path);
                return data is not null
                    ? new BehaviorResources(data)
                    : null;
            });

        return services.AddSingleton<IDictionary<string, BehaviorResources?>>(resourceMap);
    }

    // private static IServiceCollection AddCallbacks(this IServiceCollection services)
    // {
    //     
    // }
    //
    // private static IServiceCollection AddCallback(
    //     this IServiceCollection services, IEnumerable<string> callackNames)
    // {
    //     var resourceMap = callackNames.ToDictionary(
    //         callackName => callackName, callackName =>
    //         {
    //             var path = string.Format(
    //                 CallbacksPath, callackName.Split('.').Last());
    //
    //             var data = ParseJObjectFromRelativeLocation(path);
    //             return data is not null
    //                 ? new CallbackResources(data)
    //                 : null;
    //         });
    //
    //     return services.AddSingleton<IDictionary<string, CallbackResources?>>(resourceMap);
    // }
    
    private static JObject? ParseJObjectFromRelativeLocation(string relativePath)
    {
        var absolutePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);

        if (!File.Exists(absolutePath))
            return null;
            
        var json = File.ReadAllText(absolutePath);
        return JObject.Parse(json);
    }

    private static IServiceCollection AddMediatR(this IServiceCollection services) =>
        services.AddMediatR(Assembly.GetExecutingAssembly());
    
    private static IEnumerable<Type> GetImplementationsOf<T>() where T : class =>
        Assembly.GetExecutingAssembly().GetTypes()
            .Where(x => x.GetInterfaces().Contains(typeof(T)));
}