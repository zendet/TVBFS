using System.ComponentModel.DataAnnotations;

namespace TVBFS.Domain.Models.Entities;

public class TelegramUser
{
    [Key]
    [Required]
    public int Id { get; set; }
    public long? TelegramId { get; set; }
    [MaxLength(32)]
    public string? TelegramUsername { get; set; }
    
    [MaxLength(64)]
    public string? TelegramFirstName { get; set; }
    
    [MaxLength(64)]
    public string? TelegramLastName { get; set; }
    public User User { get; set; }
}