using System.ComponentModel.DataAnnotations;
using TVBFS.Domain.Models.Entities;

namespace TVBFS.Domain.Models;

public class Group
{
    [Key]
    [Required]
    public int Id { get; set; }
    
    [MaxLength(4)]
    public string Title { get; set; }
    public List<Lesson> Lessons { get; set; }
    public List<User> Students { get; set; }
}