using System.ComponentModel.DataAnnotations;

namespace TVBFS.Domain.Models.Entities;

public class VkontakteUser
{
    [Key]
    [Required]
    public int Id { get; set; }
    public string? VkName { get; set; }
    public string? VkSurname { get; set; }
    public string? VkId { get; set; }
    public string? VkRegistrationDate { get; set; }
    public User User { get; set; }
}