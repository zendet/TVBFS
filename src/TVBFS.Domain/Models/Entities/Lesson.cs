using System.ComponentModel.DataAnnotations;

namespace TVBFS.Domain.Models.Entities;

public class Lesson
{
    [Key]
    [Required]
    public int Id { get; set; }
    
    [MaxLength(140)]
    public string Title { get; set; }
    [MaxLength(10)]
    public string Classroom { get; set; } // must be numeric variable, but may have a word inside
    public byte OrdinalLessonNumber { get; set; }
    public Group Group { get; set; }
    public DateTime Date { get; set; }
    [MaxLength(5)] public string StartTime { get; set; } = null!;
    [MaxLength(5)] public string EndTime { get; set; } = null!;
}