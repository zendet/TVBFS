using System.ComponentModel.DataAnnotations;

namespace TVBFS.Domain.Models.Entities;

public class CachedCookie
{
    [Key]
    [Required]
    public int Id { get; set; }

    public string Name { get; set; }
    public string Value { get; set; }
    public string Domain { get; set; }
    public bool IsDbCookiesActual { get; set; }
}