using System.ComponentModel.DataAnnotations;

namespace TVBFS.Domain.Models.Entities;

public class User
{
    [Key]
    [Required]
    public int Id { get; set; }
    public TelegramUser? TelegramUser { get; set; }
    public VkontakteUser? VkontakteUser { get; set; }
    public Group? Group { get; set; }
    public DateTime FirstSeen { get; set; }
    public DateTime LastSeen { get; set; }
    public bool IsAutoScheduleSubscriber { get; set; }
}