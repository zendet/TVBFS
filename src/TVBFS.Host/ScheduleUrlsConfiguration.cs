namespace TVBFS.Host;

public class ScheduleUrlsConfiguration
{
    public string PlainDomain { get; set; }
    public string LoginUrl { get; set; }
    public string AllGroupsUrl { get; set; }
}