using Hangfire;
using Serilog;
using Telegram.Bot;
using TVBFS.Application.Extensions;
using TVBFS.Application.Jobs;
using TVBFS.Host.Services;
using TVBFS.Persistence.Extensions;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog();
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateLogger();

builder.Services.AddHostedService<WebhookConfigurator>();

var token = builder.Configuration["BotConfiguration:BotToken"];
builder.Services.AddHttpClient("tgwebhook")
    .AddTypedClient<ITelegramBotClient>(client => new TelegramBotClient(token, client));

builder.Services
    .AddApplication(builder.Configuration)
    .AddPersistence(builder.Configuration)
    .AddControllers()
    .AddNewtonsoftJson();

var app = builder.Build();

app.MapControllers();
app.UseRouting();
app.UseCors();

app.UseHangfireDashboard();

BackgroundJob.Enqueue<FetchGroupsJob>(job => job.Execute());
RecurringJob.AddOrUpdate<FetchGroupsJob>(job => job.Execute(), "0 0 0 ? * MON");

BackgroundJob.Enqueue<FetchScheduleJob>(job => job.Execute());

RecurringJob.AddOrUpdate<UpdateScheduleJob>(job => job.Execute(), "0 18 * * SAT-SUN");

app.Run();