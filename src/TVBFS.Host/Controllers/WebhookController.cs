using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;
using TVBFS.Application.Abstractions;

namespace TVBFS.Host.Controllers;

[ApiController]
public class WebhookController : ControllerBase
{
    [HttpPost("api/tgwebhook")]
    public async Task<IActionResult> Post(
        [FromServices] IUpdateHandler updateHandler,
        [FromBody] Update update)
    {
        var cts = new CancellationTokenSource();
        await updateHandler.HandleAsync(update);
        cts.Token.ThrowIfCancellationRequested();
        
        return Ok();
    }
}