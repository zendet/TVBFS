using Telegram.Bot;

namespace TVBFS.Host.Services;

public class WebhookConfigurator : IHostedService
{
    private readonly ILogger<WebhookConfigurator> _logger;
    private readonly IServiceProvider _services;
    private readonly IConfiguration _config;

    public WebhookConfigurator(ILogger<WebhookConfigurator> logger,
        IServiceProvider serviceProvider,
        IConfiguration config)
    {
        _logger = logger;
        _services = serviceProvider;
        _config = config;
    }
    
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        using var scope = _services.CreateScope();
        var botClient = scope.ServiceProvider.GetRequiredService<ITelegramBotClient>();

        var host = _config["BotConfiguration:HostAddress"];

        var webhookAddress = $"{host}/api/tgwebhook";
        
        _logger.LogInformation("Setting webhook: {WebhookAddress}", webhookAddress);
        await botClient.SetWebhookAsync(
            url: webhookAddress,
            dropPendingUpdates: true,
            cancellationToken: cancellationToken);
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        using var scope = _services.CreateScope();
        var botClient = scope.ServiceProvider.GetRequiredService<ITelegramBotClient>();

        _logger.LogInformation("Removing webhook");
        await botClient.DeleteWebhookAsync(cancellationToken: cancellationToken);
    }
}